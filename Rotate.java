package rotate;


import java.util.ArrayList;
import java.util.Scanner;

public class Rotate {
	
	
	public static int[] Rotate(int[] array,int x) {
		int size = array.length;
		int[] intArray = new int[size];
		
		if(x>=0){//xが正で、右にずれる
			System.arraycopy(array, 0,intArray , x%size, size-x%size);
			System.arraycopy(array, x%size+1 ,intArray , 0, x%size);
			
		}
		else if(x<0){//ｘが負で、左にずれる
			x*=-1;
			System.arraycopy(array, x%size ,intArray , 0, size-x%size);
			System.arraycopy(array, 0 ,intArray , x%size+1, x%size);
		}
		
		
		return intArray;
	}
	
	public static char[] Rotate(char[] array,int x) {
		int size = array.length;
		char[] charArray = new char[size];
		
		if(x>=0){//xが正で、右にずれる
			System.arraycopy(array, 0,charArray , x%size, size-x%size);
			System.arraycopy(array, x%size+1 ,charArray , 0, x%size);
			
		}
		else {//ｘが負で、左にずれる
			x*=-1;
			System.arraycopy(array, x%size,charArray , 0, size-x%size);
			System.arraycopy(array, 0 ,charArray , x%size+1, x%size);
		}
		
		
		return charArray;
	}
}
Yuko Yanagawa
