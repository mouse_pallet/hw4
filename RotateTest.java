package rotate;



import static org.junit.Assert.*;
import org.junit.Test;

public class RotateTest {

	@Test
	//@org.junit.Test
	public void test1(){
		assertArrayEquals("no equals", Rotate.Rotate(new int[] {1,2,3,4,5},2),new int[] {4,5,1,2,3});
		assertArrayEquals("no equals", Rotate.Rotate(new int[] {1,2,3,4,5},7),new int[] {4,5,1,2,3});
	}
	
	@Test
	//@org.junit.Test
	public void test2(){
		assertArrayEquals("no equals", Rotate.Rotate(new int[] {4,5,1,2,3},-2),new int[] {1,2,3,4,5});
		assertArrayEquals("no equals", Rotate.Rotate(new int[] {4,5,1,2,3},-7),new int[] {1,2,3,4,5});
		//assertArrayEquals("no equals", Rotate.Rotate(new int[] {1,2,3,4,5},-7),new int[] {4,5,1,2,3});
	}
	
	@Test
	//@org.junit.Test
	public void test3(){
		assertArrayEquals("no equals", Rotate.Rotate(new char[] {'a','b','c','d','e'},2),new char[] {'d','e','a','b','c'});
		assertArrayEquals("no equals", Rotate.Rotate(new char[] {'a','b','c','d','e'},7),new char[] {'d','e','a','b','c'});
		//assertArrayEquals("no equals", Rotate.Rotate(new char[] {1,2,3,4,5},7),new char[] {4,5,1,2,3});
	}

	@Test
	//@org.junit.Test
	public void test4(){
		assertArrayEquals("no equals", Rotate.Rotate(new char[] {'d','e','a','b','c'},-2),new char[] {'a','b','c','d','e'});
		assertArrayEquals("no equals", Rotate.Rotate(new char[] {'d','e','a','b','c'},-7),new char[] {'a','b','c','d','e'});
		//assertArrayEquals("no equals", Rotate.Rotate(new char[] {'a','b','c','d','e'},-7),new char[] {'d','e','a','b','c'});
		//assertArrayEquals("no equals", Rotate.Rotate(new char[] {1,2,3,4,5},7),new char[] {4,5,1,2,3});
	}
	
	

}Yuko Yanagawa
